package com.wabbitface.chluckperms;

import com.laytonsmith.PureUtilities.SimpleVersion;
import com.laytonsmith.PureUtilities.Version;
import com.laytonsmith.core.Static;
import com.laytonsmith.core.extensions.AbstractExtension;
import com.laytonsmith.core.extensions.MSExtension;

@MSExtension("CHLuckPerms")
public class Extension extends AbstractExtension {
    @Override
    public Version getVersion() {
        return new SimpleVersion("0.2.1");
    }

    @Override
    public void onStartup() {
        Static.getLogger().info("CHLuckPerms " + getVersion() + " loaded.");
    }

    @Override
    public void onShutdown() {
        Static.getLogger().info("CHLuckPerms " + getVersion() + " unloaded.");
    }
}