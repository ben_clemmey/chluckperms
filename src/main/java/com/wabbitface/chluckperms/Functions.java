package com.wabbitface.chluckperms;

import com.laytonsmith.abstraction.MCPlayer;
import com.laytonsmith.annotations.api;
import com.laytonsmith.core.Static;
import com.laytonsmith.core.constructs.CArray;
import com.laytonsmith.core.constructs.CNull;
import com.laytonsmith.core.constructs.CString;
import com.laytonsmith.core.constructs.Target;
import com.laytonsmith.core.environments.Environment;
import com.laytonsmith.core.exceptions.CRE.CREException;
import com.laytonsmith.core.exceptions.CRE.CREPlayerOfflineException;
import com.laytonsmith.core.exceptions.CRE.CREThrowable;
import com.laytonsmith.core.exceptions.ConfigRuntimeException;
import com.laytonsmith.core.natives.interfaces.Mixed;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.NodeType;
import net.luckperms.api.track.Track;

import java.util.List;
import java.util.Set;

import static com.wabbitface.chluckperms.helpers.getAPI;

@SuppressWarnings("unused")
public class Functions {
    
    public static String docs() {
        return "Luck Perms interaction functions";
    }
    
    @api
    public static class lp_get_tracks_groups extends AbsFunction {
        @SuppressWarnings("unchecked")
        public Class<? extends CREThrowable>[] thrown() {
            return new Class[]{CREException.class};
        }

        public String getName() {
            return "lp_get_tracks_groups";
        }
    
        public Integer[] numArgs() {
            return new Integer[]{0};
        }
    
        public String docs() {
            return "array {} Gets a list of groups by track.";
        }

        @Override
        public Mixed exec(Target t, Environment environment, Mixed... args) throws ConfigRuntimeException {
            Set<Track> allTracks = getAPI().getTrackManager().getLoadedTracks(); //get the tracks
            CArray result = new CArray(t,allTracks.size()); //init return array
            for (Track track : allTracks) { //loop through each track
                List<String> groups = track.getGroups(); //get track group names
                CArray trackGroups = new CArray(t,groups.size());
                for (String group : groups) { //loop through group names and add to the track group array
                    trackGroups.push(new CString(group,t),t);
                }
                result.set(track.getName(),trackGroups,t); //append array with result, track name as key
            }
            return result; //return the final array
        }
    }

    @api
    public static class lp_op_get_prefix extends AbsFunction {
        @SuppressWarnings("unchecked")
        public Class<? extends CREThrowable>[] thrown() {
            return new Class[]{CREException.class};
        }

        public String getName() {
            return "lp_op_get_prefix";
        }

        public Integer[] numArgs() {
            return new Integer[]{1};
        }

        public String docs() {
            return "string {online_player} Gets an online player's prefix or returns null.";
        }

        public Mixed exec(Target t, Environment environment, Mixed... args) throws ConfigRuntimeException {
            MCPlayer player;
            try {
                player = Static.GetPlayer(args[0].val(), t);
            }
            catch (CREPlayerOfflineException ex) {
                return CNull.NULL;
            }
            if (null == player) return CNull.NULL;
            LuckPerms api = getAPI();
            User user = api.getUserManager().getUser(player.getName());
            if (null == user) return CNull.NULL;
            String prefix = user.getCachedData().getMetaData().getPrefix();
            return new CString(prefix,t);
        }
    }
}
