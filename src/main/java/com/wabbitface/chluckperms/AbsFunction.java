package com.wabbitface.chluckperms;

import com.laytonsmith.PureUtilities.Version;
import com.laytonsmith.core.MSVersion;
import com.laytonsmith.core.functions.AbstractFunction;

public abstract class AbsFunction extends AbstractFunction {
    public boolean isRestricted() {
        return true;
    }

    public Boolean runAsync() {
        return false;
    }

    public Version since() {
        return MSVersion.V3_3_4;
    }
}
