package com.wabbitface.chluckperms;

import com.laytonsmith.core.constructs.Target;
import com.laytonsmith.core.exceptions.CRE.CREInvalidPluginException;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.LuckPermsProvider;

class helpers {
    static LuckPerms getAPI() {
        try {
            return LuckPermsProvider.get();
        }
        catch (IllegalStateException ex){
            throw new CREInvalidPluginException("LuckPerms v5.x or higher must be loaded", Target.UNKNOWN);
        }
    }
}
